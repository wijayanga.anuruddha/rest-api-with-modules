package it.codegen.demo.skill.service;

import it.codegen.demo.skill.model.Skill;

public interface SkillService
{
	public void loadSkills( Skill skill );
}
