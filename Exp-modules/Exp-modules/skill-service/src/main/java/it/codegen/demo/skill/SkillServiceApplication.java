package it.codegen.demo.skill;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class SkillServiceApplication extends SpringBootServletInitializer
{
	public static void main(String[] args) {
		SpringApplication.run(SkillServiceApplication.class, args);

	}

}
