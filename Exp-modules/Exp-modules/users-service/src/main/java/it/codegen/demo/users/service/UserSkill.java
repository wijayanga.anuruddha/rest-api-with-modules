package it.codegen.demo.users.service;

import it.codegen.demo.skill.model.Skill;
import it.codegen.demo.skill.service.SkillService;
import org.springframework.stereotype.Service;

@Service
public class UserSkill implements SkillService
{
	@Override
	public void loadSkills( Skill skill )
	{
		System.out.println( skill.getSkill() );
	}
}
