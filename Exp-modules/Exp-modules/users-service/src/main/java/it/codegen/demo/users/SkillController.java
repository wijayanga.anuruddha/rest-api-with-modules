package it.codegen.demo.users;

import it.codegen.demo.skill.model.Skill;
import it.codegen.demo.users.service.UserSkill;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SkillController
{
	@Autowired
	UserSkill skillService;

	@GetMapping("/skills")
	public String retriveSkilss()
	{
		skillService.loadSkills( new Skill( 1, "RedHat" ) );
		return "success!";
	}
}
